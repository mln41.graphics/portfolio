<?php
//foreach loop
    $a = [
        "1" => "A",
        "2" => "B",
        "3" => "C",
        "4" => "D",
        "5" => "E",
        "6" => "F",
        "7" => "G",
        "8" => "H",
    ];
    foreach($a as $index => $data){
        if ($index > 5){
            break;
        }
        echo $index." : ".$data."<br>";
    }

##########################################
//for loop
    // for ($x = 1;$x <= 20; $x++){
    //     echo $x."<br>";
    // }

##########################################
//while loop
    // $x = 2;
    // while(true){
    //     echo $x."<br>";
    //     $x = $x+2;
    // }
?>