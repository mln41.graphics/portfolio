<?php
require("DB.php");
$sql = "SELECT * FROM employee" ;
$query = mysqli_query($conn, $sql);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mali&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: 'Athiti', sans-serif;
            font-size: 20px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark  bg-dark ">
        <a class="navbar-brand" href="#"> <b> ProjectMoew </b> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.php">หน้าแรก <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="add.php">เพิ่มข้อมูล</a>
      <a class="nav-item nav-link" href="list.php">ดูข้อมูล</a>
      <!-- <a href="https://drive.google.com/drive/folders/111gHujoxnf71WEgotrhRDhWryCUMjnVe?usp=sharing" target="_blank" rel="noopener noreferrer">Goto</a> -->
                             <!-- href คือการนำทางไปยังไฟล์ที่ต้องการเชื่อม -->
        </div>
         </div>
    </nav>
    <div class="container mt-5">
    <table class="table">
  <thead>
    <tr>
      <th scope="col">ไอดี</th>
      <th scope="col">ชื่อจริง</th>
      <th scope="col">นามสกุล</th>
      <th>ควบคุม</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($query as $row){ ?>
    <tr>
        <td> <?php echo $row['id'] ?> </td>
        <td> <?php echo $row['fname'] ?> </td>
        <td> <?php echo $row['lname'] ?> </td>
        <td>  
        <a href="edit.php?id=<?php echo $row['id'] ?>" class="btn btn-primary">แก้ไข</a>
        <a href="delete.php?id=<?php echo $row['id'] ?>" class="btn btn-danger" id="delete" onclick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')"  >ลบ</a>
      </td>
     

    </tr>
    <?php  }?>
  </tbody>
</table>
</div>

</body>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <script>
              $('#delete').click(function(){
                 var conf = confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')
                 if (conf){
                    return true;
                 }else{
                    return false;  
                 }

              })
            </script>
</html>